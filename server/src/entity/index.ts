import * as OracleSession from './Common/oracle/Session';
import { Session } from './Common/mssql/Session';

export let sessionObject = {
    mssql: [Session],
    mysql: [Session],
    mariadb: [Session],
    postgres: [Session],
    oracle: [OracleSession.Session]
}
