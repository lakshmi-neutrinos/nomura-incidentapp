import * as MAPPERS from '../helper/generic/AccountMapper';
const ACCOUNT_MAPPER = MAPPERS.ACCOUNT_MAPPER;
export class Account {
    private sub;
    /**
     * 
     * @param {object | string} id username
     * @param {object} profile userinfo object
     * @param {string} type auth strategy type
     */
    constructor(id, profile, type) {
        this.sub = {
            accountId: id,
            strategy: type
        };
        Account._setProfile(profile, type, this);
    }

    static _setProfile(profile, type, instance) {
        const userClaimKeys = Object.keys(ACCOUNT_MAPPER[type]);
        userClaimKeys.forEach((key, index) => {
            if (profile.hasOwnProperty(ACCOUNT_MAPPER[type][key])) {
                instance[key] = profile[ACCOUNT_MAPPER[type][key]];
                delete profile[ACCOUNT_MAPPER[type][key]];
            }
        });
        instance['additional'] = profile;
        return instance;
    }
};