import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import { Parsers } from '../helper/parsers/parsers';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { idsutil } from './idsutil'; //_splitter_
//append_imports_end

export class ids {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'ids';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new ids(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_ids_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: ids');

    let mw_hrefstart: Middleware = new Middleware(
      this.serviceName,
      'hrefstart',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_tQ3dGC96Ldo1losZ(bh);
          //appendnew_next_mw_hrefstart
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_zY5jXL9jbYAFPYDv');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['hrefstart'] = mw_hrefstart;

    let mw_Authorize: Middleware = new Middleware(
      this.serviceName,
      'Authorize',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_2F8pU6Po9KxtZ84s(bh);
          //appendnew_next_mw_Authorize
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_695Kp5GlpT7OQ4sm');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['Authorize'] = mw_Authorize;

    //appendnew_flow_ids_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: ids');

    this.swaggerDocument['paths']['/login'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/login`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_f0bsvFvwJuouFJXW(bh);
          //appendnew_next_sd_7LyjdGvtampMFwsm
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_7LyjdGvtampMFwsm');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/login/cb'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/login/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_Jq7SEwjDHAqDCABn(bh);
          //appendnew_next_sd_9Uh8mI90N4GVjNZX
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_9Uh8mI90N4GVjNZX');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/user/info'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/user/info`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_VnANuW91nLxlHEXZ(bh);
          //appendnew_next_sd_DRgjysCaFfPwBlXi
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_DRgjysCaFfPwBlXi');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/logout'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/logout`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_eSOPbKOkYJ9DG2cW(bh);
          //appendnew_next_sd_30JipwBmsAyANPGy
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_30JipwBmsAyANPGy');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/logout/cb'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/logout/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_mjwyq4MFg7vvnmWF(bh);
          //appendnew_next_sd_ApX0usg33p84sBNd
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_ApX0usg33p84sBNd');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_ids_HttpIn
  }
  //   service flows_ids

  //appendnew_flow_ids_Start

  async sd_f0bsvFvwJuouFJXW(bh) {
    try {
      bh.local.idsConfigured = false;
      if (
        settings.default.hasOwnProperty('ids') &&
        settings.default['ids'].hasOwnProperty('client_id') &&
        settings.default['ids'].hasOwnProperty('client_secret')
      ) {
        bh.local.idsConfigured = true;
      }

      bh = await this.sd_qr2ucQkHrka1mBaF(bh);
      //appendnew_next_sd_f0bsvFvwJuouFJXW
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_f0bsvFvwJuouFJXW');
    }
  }

  async sd_qr2ucQkHrka1mBaF(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.idsConfigured,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_JxYMc38DpKTqVglR(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_rgyGKpRryMBxt5Cr(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_qr2ucQkHrka1mBaF');
    }
  }

  async sd_JxYMc38DpKTqVglR(bh) {
    try {
      bh.local.reqParams = {
        state: crypto.randomBytes(16).toString('hex'),
        nonce: crypto.randomBytes(16).toString('hex'),
        isMobile: bh.input.query.isMobile,
        redirectTo: bh.input.query.redirectTo
      };

      bh = await this.sd_OMTSr9zbnpmZDAV5(bh);
      //appendnew_next_sd_JxYMc38DpKTqVglR
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_JxYMc38DpKTqVglR');
    }
  }

  async sd_OMTSr9zbnpmZDAV5(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.reqParams;
    }
    bh = await this.sd_TMcrm2JkFcix73FR(bh);
    //appendnew_next_sd_OMTSr9zbnpmZDAV5
    return bh;
  }

  async sd_TMcrm2JkFcix73FR(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_WxXhV1ARxi5XY11z(bh);
      //appendnew_next_sd_TMcrm2JkFcix73FR
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_TMcrm2JkFcix73FR');
    }
  }
  async sd_WxXhV1ARxi5XY11z(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getAuthorizationParams();
      bh.input.authParams = outputVariables.input.authParams;

      bh = await this.sd_AvRKiAB4exMsKSGB(bh);
      //appendnew_next_sd_WxXhV1ARxi5XY11z
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_WxXhV1ARxi5XY11z');
    }
  }
  async sd_AvRKiAB4exMsKSGB(bh) {
    try {
      const authorizationRequest = Object.assign(
        {
          redirect_uri: url.resolve(bh.web.req.href, '/api/login/cb'),
          scope: 'openid profile email address phone user',
          state: bh.local.reqParams.state,
          nonce: bh.local.reqParams.nonce,
          response_type: bh.input.client.response_types[0]
        },
        bh.input.authParams
      );

      bh.local.redirectHeaders = {
        location: bh.input.client.authorizationUrl(authorizationRequest)
      };

      await this.sd_aPrUyLyAvrShAa6K(bh);
      //appendnew_next_sd_AvRKiAB4exMsKSGB
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_AvRKiAB4exMsKSGB');
    }
  }
  async sd_aPrUyLyAvrShAa6K(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_aPrUyLyAvrShAa6K');
    }
  }
  async sd_rgyGKpRryMBxt5Cr(bh) {
    try {
      bh.local.res = {
        message:
          'IDS client not registered. Register on the Neutrinos Stuido and try again'
      };

      await this.sd_PDLN1iG6KYDmbbjx(bh);
      //appendnew_next_sd_rgyGKpRryMBxt5Cr
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rgyGKpRryMBxt5Cr');
    }
  }
  async sd_PDLN1iG6KYDmbbjx(bh) {
    try {
      bh.web.res.status(404).send(bh.local.res.message);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_PDLN1iG6KYDmbbjx');
    }
  }
  async sd_tQ3dGC96Ldo1losZ(bh) {
    try {
      const protocol =
        bh.input.headers['x-forwarded-proto'] || bh.web.req.protocol;
      const href =
        protocol + '://' + bh.web.req.get('Host') + bh.web.req.originalUrl;
      bh.web.req.href = href;

      await this.sd_6wFIvxem3KW0MLNw(bh);
      //appendnew_next_sd_tQ3dGC96Ldo1losZ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_tQ3dGC96Ldo1losZ');
    }
  }
  async sd_6wFIvxem3KW0MLNw(bh) {
    try {
      bh.web.next();

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_6wFIvxem3KW0MLNw');
    }
  }

  async sd_Jq7SEwjDHAqDCABn(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.input.sessionParams = JSON.parse(
        JSON.stringify(requestObject.session)
      );
    }
    bh = await this.sd_XfMonHE4Q0yxIJ5r(bh);
    //appendnew_next_sd_Jq7SEwjDHAqDCABn
    return bh;
  }

  async sd_XfMonHE4Q0yxIJ5r(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_gPn8y5t8gezx0bcM(bh);
      //appendnew_next_sd_XfMonHE4Q0yxIJ5r
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XfMonHE4Q0yxIJ5r');
    }
  }
  async sd_gPn8y5t8gezx0bcM(bh) {
    try {
      const params = bh.input.client.callbackParams(bh.web.req);
      let tokenset = await bh.input.client.callback(
        url.resolve(bh.web.req.href, 'cb'),
        params,
        {
          nonce: bh.input.sessionParams.data.nonce,
          state: bh.input.sessionParams.data.state
        }
      );

      bh.local.redirectTo = bh.input.sessionParams.data.redirectTo;

      bh.local.userDetails = {
        tokenset: Object.assign({}, tokenset),
        userInfo: await bh.input.client.userinfo(tokenset['access_token'])
      };
      bh.local.userDetails['tokenset']['claims'] = Object.assign(
        {},
        tokenset.claims()
      );

      bh = await this.sd_r0OVnu9O0pfz8GJJ(bh);
      //appendnew_next_sd_gPn8y5t8gezx0bcM
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_gPn8y5t8gezx0bcM');
    }
  }

  async sd_r0OVnu9O0pfz8GJJ(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.userDetails;
    }
    bh = await this.sd_0A4lJtBVp8uAHdl3(bh);
    //appendnew_next_sd_r0OVnu9O0pfz8GJJ
    return bh;
  }

  async sd_0A4lJtBVp8uAHdl3(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.input.sessionParams.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_0JrZgPFq8kkCBfUb(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_ePlKadhmaieCko9S(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0A4lJtBVp8uAHdl3');
    }
  }

  async sd_0JrZgPFq8kkCBfUb(bh) {
    try {
      bh.local.htmlResponse = `
 <html>
   <script>
      let _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;

      await this.sd_dwvHWGNKjPG9R9LZ(bh);
      //appendnew_next_sd_0JrZgPFq8kkCBfUb
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0JrZgPFq8kkCBfUb');
    }
  }
  async sd_dwvHWGNKjPG9R9LZ(bh) {
    try {
      bh.web.res.status(200).send(bh.local.htmlResponse);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_dwvHWGNKjPG9R9LZ');
    }
  }
  async sd_ePlKadhmaieCko9S(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.redirectTo
      };

      await this.sd_UcMFS6skseMRcoP0(bh);
      //appendnew_next_sd_ePlKadhmaieCko9S
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ePlKadhmaieCko9S');
    }
  }
  async sd_UcMFS6skseMRcoP0(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('Redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_UcMFS6skseMRcoP0');
    }
  }

  async sd_VnANuW91nLxlHEXZ(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.session = JSON.parse(JSON.stringify(requestObject.session));
    }
    await this.sd_7RvxvdItCz0JW7yp(bh);
    //appendnew_next_sd_VnANuW91nLxlHEXZ
    return bh;
  }

  async sd_7RvxvdItCz0JW7yp(bh) {
    try {
      bh.web.res.status(200).send(bh.local.session.data.userInfo);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_7RvxvdItCz0JW7yp');
    }
  }
  async sd_NIwJNT1baHgr1cRK(bh) {
    try {
      bh.web.res.redirect('/api/login');

      //appendnew_next_sd_NIwJNT1baHgr1cRK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_NIwJNT1baHgr1cRK');
    }
  }

  async sd_eSOPbKOkYJ9DG2cW(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_Hn7EX5tpivGeeO0a(bh);
    //appendnew_next_sd_eSOPbKOkYJ9DG2cW
    return bh;
  }

  async sd_Hn7EX5tpivGeeO0a(bh) {
    try {
      bh.local.sessionExists = false;
      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset
      ) {
        bh.local.sessionData['data']['redirectTo'] =
          bh.input.query['redirectTo'];
        bh.local.sessionData['data']['isMobile'] = bh.input.query['isMobile'];
        bh.local.sessionExists = true;
      } else {
        delete bh.local.sessionData['redirectTo'];
      }

      bh = await this.sd_a5r9o9cJLyJpeUyG(bh);
      //appendnew_next_sd_Hn7EX5tpivGeeO0a
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Hn7EX5tpivGeeO0a');
    }
  }

  async sd_a5r9o9cJLyJpeUyG(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.sessionData.data;
    }
    bh = await this.sd_6EH4zDdkDu3qFoXS(bh);
    //appendnew_next_sd_a5r9o9cJLyJpeUyG
    return bh;
  }

  async sd_6EH4zDdkDu3qFoXS(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_OfViSKoyHkrQ8Xg6(bh);
      //appendnew_next_sd_6EH4zDdkDu3qFoXS
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_6EH4zDdkDu3qFoXS');
    }
  }

  async sd_OfViSKoyHkrQ8Xg6(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_Pz2WNQdjB4YzgeRd(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_l4qFGOaJ0kjIXICC(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_OfViSKoyHkrQ8Xg6');
    }
  }

  async sd_Pz2WNQdjB4YzgeRd(bh) {
    try {
      await Promise.all([
        bh.local.sessionData.data.tokenset.access_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.access_token,
              'access_token'
            )
          : undefined,
        bh.local.sessionData.data.tokenset.refresh_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.refresh_token,
              'refresh_token'
            )
          : undefined
      ]);

      bh.local.res = {
        idsURL: url.format(
          Object.assign(
            url.parse(bh.input.client.issuer.end_session_endpoint),
            {
              search: null,
              query: {
                id_token_hint: bh.local.sessionData.data.tokenset.id_token,
                post_logout_redirect_uri: url.resolve(
                  bh.web.req.href,
                  '/api/logout/cb'
                ),
                client_id: settings.default['ids']['client_id']
              }
            }
          )
        ),
        sessionExists: true
      };

      await this.sd_N0hBDMdxemPnV3no(bh);
      //appendnew_next_sd_Pz2WNQdjB4YzgeRd
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Pz2WNQdjB4YzgeRd');
    }
  }
  async sd_N0hBDMdxemPnV3no(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_N0hBDMdxemPnV3no');
    }
  }
  async sd_l4qFGOaJ0kjIXICC(bh) {
    try {
      bh.local.res = {
        sessionExists: false
      };

      await this.sd_N0hBDMdxemPnV3no(bh);
      //appendnew_next_sd_l4qFGOaJ0kjIXICC
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_l4qFGOaJ0kjIXICC');
    }
  }

  async sd_mjwyq4MFg7vvnmWF(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_Jl8hjukN3xTazsMs(bh);
    //appendnew_next_sd_mjwyq4MFg7vvnmWF
    return bh;
  }

  async sd_Jl8hjukN3xTazsMs(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      let p = function() {
        return new Promise((resolve, reject) => {
          requestObject.session.destroy(function(error) {
            if (error) {
              return reject(error);
            }
            return resolve();
          });
        });
      };
      try {
        await p();
        bh = await this.sd_Kmska1zhRX5Mtbsd(bh);
        //appendnew_next_sd_Jl8hjukN3xTazsMs
      } catch (e) {
        return await this.errorHandler(bh, e, 'sd_Jl8hjukN3xTazsMs');
      }
      return bh;
    }
  }

  async sd_Kmska1zhRX5Mtbsd(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.local.sessionData.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_HyJ4OWdwVMyKKtrm(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_Je7Us8vx3x6fsbNv(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Kmska1zhRX5Mtbsd');
    }
  }

  async sd_HyJ4OWdwVMyKKtrm(bh) {
    try {
      bh.local.res = `<html>
   <script>
      var _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;

      await this.sd_WjCw6NbEoDbSplAc(bh);
      //appendnew_next_sd_HyJ4OWdwVMyKKtrm
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_HyJ4OWdwVMyKKtrm');
    }
  }
  async sd_WjCw6NbEoDbSplAc(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_WjCw6NbEoDbSplAc');
    }
  }
  async sd_Je7Us8vx3x6fsbNv(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.sessionData.data.redirectTo
      };

      await this.sd_QAgdlJcfAu9wq2eh(bh);
      //appendnew_next_sd_Je7Us8vx3x6fsbNv
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Je7Us8vx3x6fsbNv');
    }
  }
  async sd_QAgdlJcfAu9wq2eh(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_QAgdlJcfAu9wq2eh');
    }
  }
  async sd_2F8pU6Po9KxtZ84s(bh) {
    try {
      bh.local = {};

      bh = await this.sd_2ItVNCN90o08i1Fs(bh);
      //appendnew_next_sd_2F8pU6Po9KxtZ84s
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_2F8pU6Po9KxtZ84s');
    }
  }

  async sd_2ItVNCN90o08i1Fs(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_kHFQi1Mcht1pVMkl(bh);
    //appendnew_next_sd_2ItVNCN90o08i1Fs
    return bh;
  }

  async sd_kHFQi1Mcht1pVMkl(bh) {
    try {
      bh.local.sessionExists = false;

      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset &&
        bh.local.sessionData.data.tokenset.access_token &&
        bh.local.sessionData.data.tokenset.refresh_token
      ) {
        bh.local.sessionExists = true;
      }

      bh = await this.sd_zGywluedIV5Fgbii(bh);
      //appendnew_next_sd_kHFQi1Mcht1pVMkl
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_kHFQi1Mcht1pVMkl');
    }
  }

  async sd_zGywluedIV5Fgbii(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_uvjzXNbJy9FchJVu(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_KzEtcpgUOJCFTKnp(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zGywluedIV5Fgbii');
    }
  }

  async sd_uvjzXNbJy9FchJVu(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.handleTokenExpiry(
        bh.local.sessionData,
        null
      );
      bh.local.newSession = outputVariables.input.newSession;

      bh = await this.sd_eESdmP80DLY7ImEW(bh);
      //appendnew_next_sd_uvjzXNbJy9FchJVu
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_uvjzXNbJy9FchJVu');
    }
  }

  async sd_eESdmP80DLY7ImEW(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['false'](
          bh.local.newSession,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_4rLQGWnqawNBv9u5(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_prOtwNEqzXUUyss3(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_eESdmP80DLY7ImEW');
    }
  }

  async sd_4rLQGWnqawNBv9u5(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      let p = function() {
        return new Promise((resolve, reject) => {
          requestObject.session.destroy(function(error) {
            if (error) {
              return reject(error);
            }
            return resolve();
          });
        });
      };
      try {
        await p();
        bh = await this.sd_RCMM2yUNbSkBk2c2(bh);
        //appendnew_next_sd_4rLQGWnqawNBv9u5
      } catch (e) {
        return await this.errorHandler(bh, e, 'sd_4rLQGWnqawNBv9u5');
      }
      return bh;
    }
  }

  async sd_RCMM2yUNbSkBk2c2(bh) {
    try {
      bh.local.res = {
        code: 'TOKEN_EXPIRED',
        message: 'Token invalid or access revoked'
      };

      await this.sd_WjJA2nX2IDkLbepb(bh);
      //appendnew_next_sd_RCMM2yUNbSkBk2c2
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_RCMM2yUNbSkBk2c2');
    }
  }
  async sd_WjJA2nX2IDkLbepb(bh) {
    try {
      bh.web.res.status(403).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_WjJA2nX2IDkLbepb');
    }
  }

  async sd_prOtwNEqzXUUyss3(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.newSession.rotated,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_rLvymsxs1MvSyKFK(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_g9y298tsj6WhTi6S(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_prOtwNEqzXUUyss3');
    }
  }

  async sd_rLvymsxs1MvSyKFK(bh) {
    try {
      delete bh.local.newSession.rotated;

      bh = await this.sd_XNIYhftiigmxAvaH(bh);
      //appendnew_next_sd_rLvymsxs1MvSyKFK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rLvymsxs1MvSyKFK');
    }
  }

  async sd_XNIYhftiigmxAvaH(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.newSession;
    }
    await this.sd_g9y298tsj6WhTi6S(bh);
    //appendnew_next_sd_XNIYhftiigmxAvaH
    return bh;
  }

  async sd_g9y298tsj6WhTi6S(bh) {
    try {
      bh.web.next();

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_g9y298tsj6WhTi6S');
    }
  }

  async sd_KzEtcpgUOJCFTKnp(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['cont'](
          bh.input.path,
          '/user/info',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_pv3Z3pN7PVTnGTnq(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_EnkHtgAMXr2ICfAo(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_KzEtcpgUOJCFTKnp');
    }
  }

  async sd_pv3Z3pN7PVTnGTnq(bh) {
    try {
      bh.local.res = { message: 'Session expired' };

      await this.sd_WjJA2nX2IDkLbepb(bh);
      //appendnew_next_sd_pv3Z3pN7PVTnGTnq
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_pv3Z3pN7PVTnGTnq');
    }
  }
  async sd_EnkHtgAMXr2ICfAo(bh) {
    try {
      bh.local.res = { code: 'NO_SESSION', message: 'Session not present' };

      await this.sd_WjJA2nX2IDkLbepb(bh);
      //appendnew_next_sd_EnkHtgAMXr2ICfAo
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_EnkHtgAMXr2ICfAo');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_sR2jZyQWUTi6S3WD(bh)) ||
      (await this.sd_4JosXwrHu0uuFb40(bh)) /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }

  async sd_sR2jZyQWUTi6S3WD(bh) {
    const nodes = [
      'sd_WxXhV1ARxi5XY11z',
      'sd_9Uh8mI90N4GVjNZX',
      'sd_XfMonHE4Q0yxIJ5r',
      'sd_gPn8y5t8gezx0bcM',
      'sd_Jq7SEwjDHAqDCABn',
      'sd_0A4lJtBVp8uAHdl3',
      'sd_0JrZgPFq8kkCBfUb',
      'sd_ePlKadhmaieCko9S',
      'sd_dwvHWGNKjPG9R9LZ',
      'sd_UcMFS6skseMRcoP0'
    ];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_NIwJNT1baHgr1cRK(bh);
      //appendnew_next_sd_sR2jZyQWUTi6S3WD
      return true;
    }
    return false;
  }

  async sd_4JosXwrHu0uuFb40(bh) {
    const nodes = ['sd_uvjzXNbJy9FchJVu'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_RCMM2yUNbSkBk2c2(bh);
      //appendnew_next_sd_4JosXwrHu0uuFb40
      return true;
    }
    return false;
  }

  //appendnew_flow_ids_Catch
}
