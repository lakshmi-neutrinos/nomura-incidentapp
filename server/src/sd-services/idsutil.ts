import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import { Parsers } from '../helper/parsers/parsers';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start
//append_imports_end
export class idsutil {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'idsutil';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new idsutil(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_idsutil_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: idsutil');

    //appendnew_flow_idsutil_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: idsutil');
    //appendnew_flow_idsutil_HttpIn
  }
  //   service flows_idsutil

  public async getIDSClientInstance(clientInstance = null, ...others) {
    let bh = { input: { clientInstance: clientInstance }, local: {} };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_oNufbC160vvyBWYv(bh);
      //appendnew_next_getIDSClientInstance
      //Start formatting output variables
      let outputVariables = {
        input: { clientInstance: bh.input.clientInstance },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_KtYzEMt917FCbTLd');
    }
  }
  public async getAuthorizationParams(authParams = null, ...others) {
    let bh = { input: { authParams: authParams }, local: {} };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_moHZmB3j0CGybkzl(bh);
      //appendnew_next_getAuthorizationParams
      //Start formatting output variables
      let outputVariables = {
        input: { authParams: bh.input.authParams },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_qKVsYo7Prhn4KluO');
    }
  }
  public async handleTokenExpiry(
    existingSession = '',
    newSession = '',
    ...others
  ) {
    let bh = {
      input: { existingSession: existingSession, newSession: newSession },
      local: {}
    };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_t9jxRMVTgjOFhuVY(bh);
      //appendnew_next_handleTokenExpiry
      //Start formatting output variables
      let outputVariables = {
        input: { newSession: bh.input.newSession },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_WuiShA4dV6QeVvoI');
    }
  }
  //appendnew_flow_idsutil_Start

  //new_service_variable_client
  client: any;
  async sd_oNufbC160vvyBWYv(bh) {
    try {
      bh.local.client = this['client'];
      bh = await this.sd_6EmjQIV2pQd8Vq1m(bh);
      //appendnew_next_sd_oNufbC160vvyBWYv
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_oNufbC160vvyBWYv');
    }
  }

  async sd_6EmjQIV2pQd8Vq1m(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['istype'](
          bh.local.client,
          'undefined',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_3Lr0f1hPTGSJBU0v(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_SGccX8VHXTV29rQR(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_6EmjQIV2pQd8Vq1m');
    }
  }

  async sd_3Lr0f1hPTGSJBU0v(bh) {
    try {
      const DEFAULT_HTTP_OPTIONS = {
        timeout: 60000
      };

      custom.setHttpOptionsDefaults({
        timeout: DEFAULT_HTTP_OPTIONS.timeout
      });
      log.info(
        `Identity server default HTTP options : ${DEFAULT_HTTP_OPTIONS}`
      );
      const issuer = await Issuer.discover(
        settings.default['ids']['issuerURL']
      );
      log.info(`Identity server discovered at : ${issuer.issuer}`);
      const client = await new issuer.Client({
        client_id: settings.default['ids']['client_id'],
        client_secret: settings.default['ids']['client_secret']
      });
      client[custom.clock_tolerance] = process.env.CLOCK_TOLERANCE;
      log.info('Client connected...');
      bh.input.clientInstance = client;

      bh = await this.sd_XFxrbaq1OVKRbPNs(bh);
      //appendnew_next_sd_3Lr0f1hPTGSJBU0v
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_3Lr0f1hPTGSJBU0v');
    }
  }

  async sd_XFxrbaq1OVKRbPNs(bh) {
    try {
      this['client'] = bh.input.clientInstance;
      //appendnew_next_sd_XFxrbaq1OVKRbPNs
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XFxrbaq1OVKRbPNs');
    }
  }

  async sd_SGccX8VHXTV29rQR(bh) {
    try {
      bh.input.clientInstance = this['client'];
      //appendnew_next_sd_SGccX8VHXTV29rQR
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_SGccX8VHXTV29rQR');
    }
  }
  async sd_moHZmB3j0CGybkzl(bh) {
    try {
      bh.input.authParams = {
        scope: 'openid profile email address phone offline_access user',
        prompt: 'consent'
      };

      //appendnew_next_sd_moHZmB3j0CGybkzl
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_moHZmB3j0CGybkzl');
    }
  }
  async sd_t9jxRMVTgjOFhuVY(bh) {
    try {
      const tokenset = bh.input.existingSession.data.tokenset;
      bh.local.expires_at = tokenset['expires_at'] * 1000;
      bh.local.refreshTime = new Date().valueOf() + 300000; // 5min before

      bh = await this.sd_04d7TC2aabJRcuAb(bh);
      //appendnew_next_sd_t9jxRMVTgjOFhuVY
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_t9jxRMVTgjOFhuVY');
    }
  }

  async sd_04d7TC2aabJRcuAb(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['gt'](
          bh.local.expires_at,
          bh.local.refreshTime,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_tUh8Uh3Cqf4lDfWM(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_xxmYuYdzeLO8D9rf(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_04d7TC2aabJRcuAb');
    }
  }

  async sd_tUh8Uh3Cqf4lDfWM(bh) {
    try {
      bh.input.newSession = bh.input.existingSession.data;

      //appendnew_next_sd_tUh8Uh3Cqf4lDfWM
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_tUh8Uh3Cqf4lDfWM');
    }
  }
  async sd_xxmYuYdzeLO8D9rf(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_kJiaUoLXUvPRoNlD(bh);
      //appendnew_next_sd_xxmYuYdzeLO8D9rf
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_xxmYuYdzeLO8D9rf');
    }
  }
  async sd_kJiaUoLXUvPRoNlD(bh) {
    try {
      bh.local.refresh_token = await bh.input.client.introspect(
        bh.input.existingSession.data.tokenset.refresh_token,
        'refresh_token'
      );

      bh = await this.sd_xhyQE8jEA8iTkrTw(bh);
      //appendnew_next_sd_kJiaUoLXUvPRoNlD
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_kJiaUoLXUvPRoNlD');
    }
  }

  async sd_xhyQE8jEA8iTkrTw(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.refresh_token.active,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_wSelRMMUarvEZ8Ak(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_G863GPYHiTwwAEed(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_xhyQE8jEA8iTkrTw');
    }
  }

  async sd_wSelRMMUarvEZ8Ak(bh) {
    try {
      bh.input.newSession = { rotated: true };
      bh.input.newSession['tokenset'] = await bh.input.client.refresh(
        bh.input.existingSession.data.tokenset.refresh_token
      );
      bh.input.newSession['userInfo'] = await bh.input.client.userinfo(
        bh.input.newSession['tokenset']['access_token']
      );
      bh.input.newSession['tokenset']['claims'] = Object.assign(
        {},
        bh.input.newSession['tokenset'].claims()
      );

      //appendnew_next_sd_wSelRMMUarvEZ8Ak
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_wSelRMMUarvEZ8Ak');
    }
  }
  async sd_G863GPYHiTwwAEed(bh) {
    try {
      bh.input.newSession = false;

      //appendnew_next_sd_G863GPYHiTwwAEed
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_G863GPYHiTwwAEed');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_1GH8VNlaAdskq5e3(bh)) /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }

  async sd_1GH8VNlaAdskq5e3(bh) {
    const nodes = ['handleTokenExpiry'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_G863GPYHiTwwAEed(bh);
      //appendnew_next_sd_1GH8VNlaAdskq5e3
      return true;
    }
    return false;
  }

  //appendnew_flow_idsutil_Catch
}
