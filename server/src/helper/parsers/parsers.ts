import * as xml2js from 'xml2js';
import * as yaml from 'js-yaml';

export class Parsers {

    xml(value, parserOptions, attrkey, charkey) {
        return new Promise((resolve, reject) => {
            try {
                let parseString = xml2js.parseString;
                if (value !== undefined) {
                    var options;
                    if (typeof value === "object") {
                        options = { renderOpts: { pretty: false } };
                        if (parserOptions && typeof parserOptions === "object") { options = parserOptions; }
                        options.async = false;
                        var builder = new xml2js.Builder(options);
                        value = builder.buildObject(value, options);
                        return resolve(value);
                    }
                    else if (typeof value == "string") {
                        options = {};
                        if (parserOptions && typeof parserOptions === "object") { options = parserOptions; }
                        options.async = true;
                        options.attrkey = attrkey || options.attrkey || '$';
                        options.charkey = charkey || options.charkey || '_';
                        parseString(value, options, function (err, result) {
                            if (err) { return reject(err) }
                            else {
                                return resolve(result);
                            }
                        });
                    }
                }
                else {
                    return reject(new Error("XML Parser :: value cannot undefined"));
                }
            }
            catch (err) {
                return reject(err);
            }
        });
    }

    json(payload) {
        return new Promise((resolve, reject) => {
            if (payload !== undefined) {
                if (typeof payload === 'string') {
                    try {
                        payload = JSON.parse(payload);
                        return resolve(payload);
                    } catch (error) {
                        return reject(error);
                    }
                } else if (typeof payload === 'object') {
                    try {
                        payload = JSON.stringify(payload);
                        return resolve(payload);
                    } catch (error) {
                        return reject(error);
                    }
                } else {
                    return reject(new Error('Invalid Arguments. JSON convertor takes either a valid JSON object or stringified JSON'));
                }
            } else {
                return reject(new Error('Invalid arguments'));
            }
        });
    }

    html() {

    }

    csv() {

    }

    yml(payload) {
        return new Promise((resolve, reject) => {
            if (payload !== undefined) {
                if (typeof payload === "string") {
                    try {
                        payload = yaml.load(payload);
                        return resolve(payload);
                    }
                    catch (e) {
                        return reject(e);
                    }
                }
                else if (typeof payload === "object") {
                    if (!Buffer.isBuffer(payload)) {
                        try {
                            payload = yaml.dump(payload);
                            return resolve(payload);
                        }
                        catch (e) {
                            return reject(e);
                        }
                    }
                    else {
                        return reject(new Error('Invalid YAML object'));
                    }
                }
                else {
                    return reject(new Error('YAML convertor takes either a valid YAML string to convert to JSON or a JSON to convert to YAML'));
                }
            } else {
                return reject(new Error('Invalid arguments'));
            }
        })
    }
}