export const environment = {
    "name": "dev",
    "properties": {
        "production": false,
        "ssdURL": "http://localhost:8081/api/",
        "tenantName": "nomura",
        "appName": "nomura-incidentapp",
        "namespace": "com.nomura.nomura-incidentapp",
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "useDefaultExceptionUI": true,
        "isIDSEnabled": "true"
    }
}